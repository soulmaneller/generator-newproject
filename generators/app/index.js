(function () {
    "use strict";
    var generators = require('yeoman-generator'),
        path = require('path');

    var methods = {};

    methods.constructor = function() {
        generators.Base.apply( this, arguments );

        this.option('root');
    };

    methods.main = function( appName ) {
        var _this = this,
            done = this.async(),
            isRoot = this.options.root ? true : false;

        var _defaultDirName = process.cwd().split(path.sep).pop();
        var qData = [
                {
                    type: "input",
                    name: "p_name",
                    message: "Enter project name :",
                    default: appName || _defaultDirName
                },
                {
                    type: "confirm",
                    name: "is_root",
                    message: "Generate this directory as root?",
                    default: function( answers ) {
                        if( isRoot === true ) {
                            return true;
                        }

                        return ( answers.p_name == _defaultDirName );
                    }
                },
                {
                    type: "confirm",
                    name: 'add_test',
                    message: "Would do you want to include test?",
                    default: true,
                },
                {
                    type: "list",
                    name: "test_module",
                    message: "Select framework of test",
                    choices: [ 'mocha', 'jasmine' ],
                    when: function( answers ) {
                        return answers.add_test;
                    }
                },
                {
                    type: "checkbox",
                    name: "optional",
                    message: "Select options for initial your project",
                    choices: [
                        'grunt-contrib-connect',
                        'grunt-concurrent',
                        'grunt-nodemon',
                        'grunt-contrib-less',
                        'grunt-wiredep',
                        'grunt-injector',
                        'grunt-contrib-concat',
                        'grunt-contrib-clean',
                        'grunt-contrib-uglify',
                    ]
                }
            ];

        this.prompt( qData ).then( function( answers ) {
            var dev = [],
                destRoot = answers.is_root ? '.' : answers.p_name;

            _this.appName = answers.p_name;
            _this.destinationRoot( destRoot );
            _this.options.answers = answers;

            done();
        });
    };

    methods.manageOptions = function() {
        var ans = this.options.answers,
            dev = [];

        if( ans.add_test ) {
            if( ans.test_module == 'mocha' ) {
                dev.push( "grunt-mocha-cli", "mocha", "chai" );
                this.gruntfile.insertConfig(
                    "mochacli",
                    "{ options: { require: ['chai'], reporter: 'nyan', bail: true }, all: ['test/*.js'] }"
                );
            } else {
                dev.push( "grunt-contrib-jasmine" );
            }
        }

        dev = dev.concat( ans.optional );
        this.options.dev = dev;
    };

    methods.copy = function() {
        this.fs.copyTpl(
            this.templatePath(),
            this.destinationPath(),
            this
        );

        this.fs.copy(
            this.templatePath('.bowerrc'),
            this.destinationPath('.bowerrc')
        );
        this.fs.copy(
            this.templatePath('.hgignore'),
            this.destinationPath('.hgignore')
        );
        this.fs.copy(
            this.templatePath('.gitignore'),
            this.destinationPath('.gitignore')
        );
    };

    methods.createGrunt = function() {
        var ans = this.options.answers,
            gConf = {
                watch: {
                    gruntfile: {
                        files: [ 'Gruntfile.js' ],
                        options: {
                            reload: true
                        }
                    }
                },
                mochacli: {
                    options: {
                        require: [ 'chai' ],
                        reporter: 'nyan',
                        bail: true
                    },
                    test: ['tests/*spec.js']
                },

                jasmine: {
                    pivotal: {
                        src: 'src/**/*.js',
                        options: {
                            specs: 'tests/*Spec.js',
                            helpers: 'tests/*Helper.js'
                        }
                    }
                },

                connect: {
                    server: {
                        options: {
                            port: 3001,
                            base: '.'
                        }
                    }
                }
            },
            _defaultTasks = [ 'watch' ];

        // If adding test
        if( ans.add_test ) {
            var testModule = ( ans.test_module == 'mocha' ? 'mochacli' : 'jasmine' );

            gConf.watch.test = {
                files: [ 'tests/**/*.js' ],
                tasks: [ testModule ]
            };

            this.gruntfile.insertConfig( testModule, JSON.stringify( gConf[ testModule ], null, '  ' ));
            this.gruntfile.registerTask( 'test', [ testModule ]);
            _defaultTasks.unshift( testModule );
        }

        // If add grunt-contrib-connect
        if( this.options.dev.indexOf( 'grunt-contrib-connect' ) !== -1 ) {
            this.gruntfile.insertConfig( "connect", JSON.stringify( gConf.connect, null, '  ' ));
        }

        this.gruntfile.prependJavaScript("require('matchdep').filterDev('grunt-*').forEach(grunt.loadNpmTasks);");
        this.gruntfile.insertConfig( "watch", JSON.stringify( gConf.watch, null, '  ' ));
        this.gruntfile.registerTask( 'default', _defaultTasks );
        this.fs.write( this.destinationPath( "Gruntfile.js" ), this.gruntfile.toString() );
    };

    methods.install = function() {
        var opts = {
                stdio: 'inherit'
            },
            devDependencies = [
                'add',
                '--dev',
                'grunt',
                'grunt-contrib-watch',
                'matchdep'
            ],
            dependencies = [
                'add',
                'fs-extra',
                'lodash',
            ];

        devDependencies = devDependencies.concat( this.options.dev );

        this.spawnCommandSync('git', [ 'init' ], opts );
        this.spawnCommandSync('hg', [ 'init' ], opts );
        this.spawnCommandSync('yarn', dependencies, opts );
        this.spawnCommandSync('yarn', devDependencies, opts );
    };

    module.exports = generators.Base.extend( methods );
}());
