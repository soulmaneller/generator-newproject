(function () {
    "use strict";
    var fs = require('fs-extra'),
        path = require('path'),
        _ = require('lodash');

    var configPath = "config/config.json",
        _defaultConfig = {},
        config;

    if( fs.existsSync( configPath )) {
        try {
            config = fs.readJSONSync( configPath );
        } catch (e) {
            config = {};
        }

        config = _.merge( _defaultConfig, config );
    } else {
        config = _defaultConfig;

        fs.outputJsonSync( configPath, config );
    }

    module.exports = config;
}());
